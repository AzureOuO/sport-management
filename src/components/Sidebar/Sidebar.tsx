import React from "react";

const Sidebar: React.FC = () => (
  <div className="flex">
    <div className="flex-auto">Sidebar Component</div>
  </div>
);

export default Sidebar;
