import React from "react";
import "./App.css";
import Sidebar from "./components/Sidebar/Sidebar";

function App() {
  return (
    <div className="flex flex-row justify-around items-stretch h-screen bg-gray-500">
      <div className="border-r-2">
        <Sidebar></Sidebar>
      </div>
      <div className="border-r-2">Main1</div>
      <div>Blocks</div>
    </div>
  );
}

export default App;
